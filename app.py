from flask import Flask, render_template, request, redirect, url_for
from datetime import datetime


# appという名前でインスタンス化
app = Flask(__name__)

#================================
# 変数定義
#================================
# イメージを保持
chat_images = []
chat_images.append("../static/images/aragaki_talk.gif")
chat_images.append("../static/images/aragaki_angry.gif")
chat_images.append("../static/images/aragaki_happy.gif")
chat_images.append("../static/images/aragaki_suprise.gif")
chat_images.append("../static/images/aragaki_empty.gif")

response_titles = []
response_box = []